package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var integrationTests = []struct {
	jsonPath string
}{
	{"test_files/push_event.json"},
	{"test_files/merge_event.json"},
	{"test_files/pipeline_event_running.json"},
	{"test_files/pipeline_event_completed.json"},
}
var a App

func TestMain(m *testing.M) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Hello")
	}))
	defer ts.Close()
	os.Setenv("APP_PORT", "8001")
	os.Setenv("MSG_API_URL", ts.URL)
	a = App{}
	a.Initialize()
	code := m.Run()
	log.Println("code", code)

	os.Exit(code)
}

func TestHealthCheck(t *testing.T) {
	req, _ := http.NewRequest("GET", "/health", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestHandleWebhook(t *testing.T) {

	for _, test := range integrationTests {
		payload, err := ioutil.ReadFile(test.jsonPath)
		if err != nil {
			t.Errorf("Could not load JSON %s", err)
		}

		req, _ := http.NewRequest("POST", "/api/v1/events/webex_teams/test-room",
			bytes.NewBuffer(payload))
		response := executeRequest(req)

		checkResponseCode(t, http.StatusOK, response.Code)
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}
