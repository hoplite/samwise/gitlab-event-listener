package main

import (
	"encoding/json"
	"io/ioutil"
	"testing"

	"github.com/google/go-cmp/cmp"
)

var messageTests = []struct {
	jsonPath string
	service  string
	roomID   string
	out      Message
}{
	{"test_files/pipeline_event_running.json", "webex_teams", "test123",
		Message{Service: "webex_teams", RoomID: "test123",
			MessageText: "", MarkdownText: "**Gitlab Event:**\n\n**Administrator** started a pipeline in project [Gitlab Test](http://192.168.64.1:3005/gitlab-org/gitlab-test)\n\n**Status:** [running](http://192.168.64.1:3005/gitlab-org/gitlab-test/pipelines/31)\n\n**Runner**: us-west-1-runner-docker\n\n"}},
	{"test_files/pipeline_event_completed.json", "webex_teams", "test123",
		Message{Service: "webex_teams", RoomID: "test123",
			MessageText: "", MarkdownText: "**Gitlab Event:**\n\n**Pipeline completed:**\n\n**Project:** [Gitlab Test](http://192.168.64.1:3005/gitlab-org/gitlab-test)\n\n**Status:** [success](http://192.168.64.1:3005/gitlab-org/gitlab-test/pipelines/31)\n\n"}},
	{"test_files/push_event.json", "webex_teams", "test123",
		Message{Service: "webex_teams", RoomID: "test123",
			MessageText: "", MarkdownText: "**Gitlab Event:**\n\nUser: **John Smith**\n\nPushed **4** commits to [Diaspora](http://example.com/mike/diaspora):\n\nBranch: refs/heads/master\n\n- **Commit:** [Update Catalan translation to e38cb41.](http://example.com/mike/diaspora/commit/b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327)\n- **Commit:** [fixed readme](http://example.com/mike/diaspora/commit/da1560886d4f094c3e6c9ef40349f7d38b5d27d7)\n"}},
	{"test_files/merge_event.json", "webex_teams", "test123",
		Message{Service: "webex_teams", RoomID: "test123",
			MessageText: "", MarkdownText: "**Gitlab Event:**\n\nA merge request was **opened** by user: **Administrator** in project [Gitlab Test](http://example.com/gitlabhq/gitlab-test):\n\n**Merge request:** [MS-Viewport](http://example.com/diaspora/merge_requests/1)\n\n**Source branch:** ms-viewport\n\n**Target branch:** master\n\n"}},
}

func TestCreateMessage(t *testing.T) {

	for _, test := range messageTests {

		payload, err := ioutil.ReadFile(test.jsonPath)
		if err != nil {
			t.Errorf("Could not load JSON %s", err)
		}
		var webhook map[string]interface{}
		if err := json.Unmarshal(payload, &webhook); err != nil {
			t.Errorf("Could not unmarshall JSON")
		}

		ge, _ := gitLabEventType(webhook)
		m := ge.CreateMessage(test.service)
		m.RoomID = test.roomID
		if !cmp.Equal(m, test.out) {
			t.Errorf("GitlabObject is not correct, got %v, want %v", m, test.out)
		}
	}
}
