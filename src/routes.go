package main

func (app *App) InitializeRoutes() {
	app.Router.HandleFunc("/api/v1/events/{service}/{roomID}", handleWebhook).Methods("POST")
	app.Router.HandleFunc("/health", Health).Methods("GET")
}
