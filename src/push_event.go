package main

import (
	"bytes"
	"fmt"
	"log"
	"strconv"
	"strings"
)

type PushEvent struct {
	ObjectKind   string     `mapstructure:"object_kind"`
	Ref          string     `mapstructure:"ref"`
	User         string     `mapstructure:"user_name"`
	UserName     string     `mapstructure:"user_username"`
	UserEmail    string     `mapstructure:"user_email"`
	Project      Project    `mapstructure:"project,squashed"`
	Repo         Repository `mapstructure:"repository,omitempty,squashed"`
	Commits      []Commit   `mapstructure:"commits,squashed"`
	TotalCommits int        `mapstructure:"total_commits_count"`
}

func (p PushEvent) CreateMessage(service string) Message {
	var b bytes.Buffer
	var message Message
	var messageText string

	message.Service = service

	messageText = fmt.Sprintf(
		"**Gitlab Event:**\n\n"+
			"User: **%s**\n\n"+
			"Pushed **%s** commits to [%s](%s):\n\n"+
			"Branch: %s\n\n",
		p.User, strconv.Itoa(p.TotalCommits),
		p.Repo.Name, p.Repo.HomePage, p.Ref)

	b.WriteString(messageText)
	if p.TotalCommits > 0 {
		for _, commit := range p.Commits {
			commitSubject := strings.Split(commit.Message, "\n")[0]
			commitMsg := fmt.Sprintf(
				"- **Commit:** [%s](%s)\n",
				commitSubject, commit.Url)
			b.WriteString(commitMsg)
		}
	}
	message.MarkdownText = b.String()
	log.Println("Push event message text:", message.MarkdownText)

	return message
}
