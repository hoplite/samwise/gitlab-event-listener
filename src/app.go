package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/mitchellh/mapstructure"
)

type GitlabObject interface {
	CreateMessage(string) Message
}

type Project struct {
	Id   int    `mapstructure:"id"`
	Name string `mapstructure:"name"`
	Url  string `mapstructure:"web_url"`
}

type Repository struct {
	HomePage string `mapstructure:"homepage,omitempty"`
	Url      string `mapstructure:"url,omitempty"`
	Name     string `mapstructure:"name,omitempty"`
}

type Author struct {
	Name  string `mapstructure:"name,omitempty"`
	Email string `mapstructure:"email,omitempty"`
}

type Commit struct {
	Id        string   `mapstructure:"id"`
	Message   string   `mapstructure:"message"`
	TimeStamp string   `mapstructure:"timestamp,omitempty"`
	Author    *Author  `mapstructure:"author,squashed"`
	Url       string   `mapstructure:"url"`
	Added     []string `mapstructure:"added,omitempty"`
	Modified  []string `mapstructure:"modified,omitempty"`
	Removed   []string `mapstructure:"removed,omitempty"`
}

type User struct {
	Name     string `mapstructure:"name,omitempty"`
	UserName string `mapstructure:"username"`
}

type Message struct {
	Service      string
	RoomID       string
	MessageText  string
	MarkdownText string
}

type App struct {
	Router *mux.Router
}

func failOnError(err error, msg string) {
	if err != nil {
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func (a *App) Initialize() {
	log.Println("Starting service..")
	a.Router = mux.NewRouter()
	a.InitializeRoutes()
}

// Run starts the app and serves on the specified addr
func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}

func Health(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("OK"))
}

func gitLabEventType(w map[string]interface{}) (g GitlabObject, err error) {
	// Decode the map of interfaces into a given struct based on the
	// object_kind key.
	switch w["object_kind"] {
	case "push":
		var p PushEvent
		log.Println("decoding map w", w)
		err = mapstructure.Decode(w, &p)
		if err != nil {
			failOnError(err,
				"Error decoding the mapstructure into a push event")
		} else {
			g = p
			log.Println("decoded pushEvent", p)
		}
	case "merge_request":
		var mr MergeRequestEvent
		log.Println("decoding map w", w)
		err = mapstructure.Decode(w, &mr)
		if err != nil {
			failOnError(err,
				"Error decoding the mapstructure into a merge request")
		} else {
			g = mr
			log.Println("decoded pipeline", mr)
		}
	case "pipeline":
		var pipe PipelineEvent
		log.Println("decoding map w", w)
		err = mapstructure.Decode(w, &pipe)
		if err != nil {
			failOnError(err,
				"Error decoding the mapstructure into a pipeline")
		} else {
			g = pipe
			log.Println("decoded pi", pipe)
		}
	default:
		err = fmt.Errorf(
			"JSON error: object_kind could not be determined! object_kind %s",
			w["object_kind"])
	}
	return g, err
}

func handleWebhook(w http.ResponseWriter, r *http.Request) {

	log.Printf("Received request on URL: %s from host %s\n", r.URL, r.RemoteAddr)
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))

	if err != nil {
		failOnError(err, "Could not read request body")
	}

	if err := r.Body.Close(); err != nil {
		failOnError(err, "Could not close io reader")
	}

	var webhook map[string]interface{}

	if err := json.Unmarshal(body, &webhook); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessablle entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			failOnError(err, "Error Unmarshalling JSON")
		}
	}

	ge, err := gitLabEventType(webhook)
	if err != nil {
		failOnError(err,
			"gitlabObject could not be created! JSON may be incorrect!")
		return
	}

	params := mux.Vars(r)
	log.Printf("params: %+v\n", params)
	log.Println("RoomID: ", params["roomID"])
	log.Printf("GitlabObject: %+v\n", ge)
	m := ge.CreateMessage(params["service"])
	m.RoomID = params["roomID"]

	msgApi := os.Getenv("MSG_API_URL")
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(m)

	log.Println("Sending message to", msgApi)
	res, err := http.Post(msgApi,
		"application/json; charset=utf-8", b)
	if err != nil {
		failOnError(err, "Could not send message to message api")
	}

	io.Copy(os.Stdout, res.Body)
}
