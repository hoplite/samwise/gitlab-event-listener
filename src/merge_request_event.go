package main

import (
	"bytes"
	"fmt"
	"log"

	"github.com/google/go-cmp/cmp"
)

type MergeRequestEvent struct {
	ObjectKind string                 `mapstructure:"object_kind"`
	Attributes MergeRequestAttributes `mapstructure:"object_attributes,squashed"`
	Project    Project                `mapstructure:"project,squashed"`
	Repo       Repository             `mapstructure:"repository,omitempty,squashed"`
	User       User                   `mapstructure:"user,squashed"`
	LastCommit Commit                 `mapstructure:"last_commit,squashed"`
	Wip        bool                   `mapstructure:"work_in_progress"`
	Url        string                 `mapstructure:"url"`
}

type MergeRequestAttributes struct {
	Id           int    `mapstructure:"id"`
	TargetBranch string `mapstructure:"target_branch"`
	SourceBranch string `mapstructure:"source_branch"`
	Title        string `mapstructure:"title,omitempty"`
	State        string `mapstructure:"state"`
	MergeStatus  string `mapstructure:"merge_status"`
	Created      string `mapstructure:"created_at,omitempty"`
	Updated      string `mapstructure:"updated_at,omitempty"`
	Url          string `mapstructure:"url"`
}

func (m MergeRequestEvent) CreateMessage(service string) Message {
	var b bytes.Buffer
	var message Message
	var messageText string
	var mergeUser string

	message.Service = service

	// Test that the pipeline user is really a User struct
	if cmp.Equal(m.User, User{}) == false {
		if m.User.Name != "" {
			mergeUser = m.User.Name
		} else {
			mergeUser = m.User.UserName
		}
	}

	messageText = fmt.Sprintf(
		"**Gitlab Event:**\n\n"+
			"A merge request was **%s** by user: **%s** in project [%s](%s):\n\n"+
			"**Merge request:** [%s](%s)\n\n"+
			"**Source branch:** %s\n\n"+
			"**Target branch:** %s\n\n",
		m.Attributes.State, mergeUser, m.Repo.Name, m.Repo.HomePage,
		m.Attributes.Title, m.Attributes.Url, m.Attributes.SourceBranch,
		m.Attributes.TargetBranch)
	b.WriteString(messageText)

	message.MarkdownText = b.String()
	log.Println("Merge request message text", message.MarkdownText)

	return message
}
