package main

import (
	"bytes"
	"fmt"
	"log"
	"strconv"

	"github.com/google/go-cmp/cmp"
)

type PipelineEvent struct {
	ObjectKind string             `mapstructure:"object_kind"`
	Attributes PipelineAttributes `mapstructure:"object_attributes,squashed"`
	Project    Project            `mapstructure:"project,squashed"`
	Repo       Repository         `mapstructure:"repository,omitempty,squashed"`
	User       User               `mapstructure:"user,squashed"`
	Builds     []PipelineBuild    `mapstructure:"builds,squashed"`
	Commit     Commit             `mapstructure:"commit,squashed"`
}

type PipelineAttributes struct {
	Id       int    `mapstructure:"id"`
	Ref      string `mapstructure:"ref"`
	Sha      string `mapstructure:"sha"`
	Started  string `mapstructure:"started_at,omitempty"`
	Finished string `mapstructure:"finished_at,omitempty"`
	Status   string `mapstructure:"status"`
}

type PipelineBuild struct {
	Id       int    `mapstructure:"id"`
	Name     string `mapstructure:"name"`
	Stage    string `mapstructure:"stage"`
	Status   string `mapstructure:"status"`
	User     User   `mapstructure:"user,omitempty,squashed"`
	Created  string `mapstructure:"created_at,omitempty"`
	Started  string `mapstructure:"started_at,omitempty"`
	Finished string `mapstructure:"finished_at,omitempty"`
	Runner   Runner `mapstructure:"runner,omitempty,squashed"`
}

type Runner struct {
	Id          int    `mapstructure:"id"`
	Description string `mapstructure:"description,omitempty"`
}

func IsFinished(status string) bool {
	finishedStates := map[string]bool{
		"failed":    true,
		"cancelled": true,
		"success":   true,
		"manual":    true,
		"skipped":   true,
	}

	return finishedStates[status]
}

func (p PipelineEvent) CreateMessage(service string) Message {
	var b bytes.Buffer
	var message Message
	var pipelineUser string
	var messageText string

	message.Service = service

	resultsURL := fmt.Sprintf("%s/pipelines/%s",
		p.Project.Url, strconv.Itoa(p.Attributes.Id))
	log.Printf("Results url: %+v\n", resultsURL)

	// Test that the pipeline user is really a User struct
	if cmp.Equal(p.User, User{}) == false {
		if p.User.Name != "" {
			pipelineUser = p.User.Name
		} else {
			pipelineUser = p.User.UserName
		}
	}

	// If the pipeline is finished, use the completed message string.
	if IsFinished(p.Attributes.Status) {
		messageText = fmt.Sprintf(
			"**Gitlab Event:**\n\n"+
				"**Pipeline completed:**\n\n"+
				"**Project:** [%s](%s)\n\n"+
				"**Status:** [%s](%s)\n\n",
			p.Project.Name, p.Project.Url, p.Attributes.Status, resultsURL)
		b.WriteString(messageText)
	} else {
		log.Printf("First build: %+v\n", p.Builds[0])
		runner := p.Builds[0].Runner.Description
		messageText = fmt.Sprintf(
			"**Gitlab Event:**\n\n"+
				"**%s** started a pipeline in project [%s](%s)\n\n"+
				"**Status:** [%s](%s)\n\n",
			pipelineUser, p.Project.Name, p.Project.Url,
			p.Attributes.Status, resultsURL)
		b.WriteString(messageText)
		// Tell the user what runner is being used if the pipeline has started
		if runner != "" {
			runnerText := fmt.Sprintf("**Runner**: %s\n\n", runner)
			b.WriteString(runnerText)
		}
	}

	message.MarkdownText = b.String()
	log.Println("Pipeline event message text", message.MarkdownText)

	return message
}
