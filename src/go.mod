module gitlab.com/hoplite/samwise/gitlab-event-listener

go 1.15

require (
	github.com/google/go-cmp v0.5.4
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/mapstructure v1.4.0
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
