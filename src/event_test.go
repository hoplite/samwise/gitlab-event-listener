package main

import (
	"encoding/json"
	"io/ioutil"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/mitchellh/mapstructure"
)

func TestPushEventType(t *testing.T) {

	payload, err := ioutil.ReadFile("test_files/push_event.json")
	if err != nil {
		t.Errorf("Could not load JSON %s", err)
	}

	var webhook map[string]interface{}
	var p PushEvent
	if err := json.Unmarshal(payload, &webhook); err != nil {
		t.Errorf("Could not unmarshall JSON")
	}

	mapstructure.Decode(webhook, &p)
	ge, _ := gitLabEventType(webhook)
	if !cmp.Equal(ge, p) {
		t.Errorf("GitlabObject is not a PushEvent, got %v, want %v", ge, p)
	}
}

func TestMergeRequestEventType(t *testing.T) {

	payload, err := ioutil.ReadFile("test_files/merge_event.json")
	if err != nil {
		t.Errorf("Could not load JSON %s", err)
	}

	var webhook map[string]interface{}
	var m MergeRequestEvent
	if err := json.Unmarshal(payload, &webhook); err != nil {
		t.Errorf("Could not unmarshall JSON")
	}

	mapstructure.Decode(webhook, &m)
	ge, _ := gitLabEventType(webhook)
	if !cmp.Equal(ge, m) {
		t.Errorf("GitlabObject is not a PushEvent, got %v, want %v", ge, m)
	}
}

func TestPipelineEventType(t *testing.T) {

	payload, err := ioutil.ReadFile("test_files/pipeline_event_running.json")
	if err != nil {
		t.Errorf("Could not load JSON %s", err)
	}

	var webhook map[string]interface{}
	var p PipelineEvent
	if err := json.Unmarshal(payload, &webhook); err != nil {
		t.Errorf("Could not unmarshall JSON")
	}

	mapstructure.Decode(webhook, &p)
	ge, _ := gitLabEventType(webhook)
	if !cmp.Equal(ge, p) {
		t.Errorf("GitlabObject is not a correct, got %v, want %v", ge, p)
	}
}
