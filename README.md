# Samwise project: gitlab-event-listener

The gitlab-event-listener is a microservice that listens for GitLab events like branch creations, pull requests, and trigger events.
When the service receives a webhook from a GitLab server, it posts a message to an instance of the message-api service.

# Overview

The diagram below is an example configuration of the Samwise services, and how the gitlab-event-listener currently fits in the larger collection of services.

![](images/samwise-overview.png)

## Supported GitLab Events
**Currently this service can handle the following events:**
- [Push events](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#push-events)
- [Merge request events](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#merge-request-events)
- [Pipeline events](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#pipeline-events)

**The following events are unsupported:**
- [Tag events](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#tag-events)
- [Issues events](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#issues-events)
- [Comment events](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#comment-events)
- [Wiki page events](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#wiki-page-events)
- [Build events](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#build-events)

Some or all of these will be added in the future as needed.

## Environment variables
This service is a [12-factor app](https://12factor.net/), and the following environment variables are needed for the service to work properly.
- APP_PORT: Sets the port that the service will run on.
    Ex: APP_PORT=8001
- MSG_API_URL: The URL of a running message-api service. https://gitlab.com/hoplite/samwise/message-api
    Ex: MSG_API_URL=http://localhost:8000/api/v1
## Using the service
The service has two endpoints. One for health checks, and the other is the api where webhooks payloads are sent.

```
/health
  Method: GET
  Response: 200, ''
/api/v1/events/<service>/<roomname>
  Method: POST
  Response: 200 ''
  Example: http://localhost/api/v1/events/webex_teams/test-roomname
```
The service and room names in the services API correspond with service rabbitmq queues setup between the [message-api](https://gitlab.com/hoplite/samwise/message-api) service and a relay service like the [webex-teams-relay](https://gitlab.com/hoplite/samwise/webex-teams-relay) service. The room name is merely the name of the room a user would like the events to go to.

Pointing to the service can be done by setting a webhook integration in a GitLab project:
![](images/webhook-integration.png)

**A resulting message in Webex Teams would look like this:**
![](images/message-example.png)
